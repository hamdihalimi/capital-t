import React from 'react'
import { Col, Container, Row } from 'react-bootstrap'
import { FaFacebookF,FaTwitterSquare} from 'react-icons/fa';
import {FiInstagram } from 'react-icons/fi'
import { SocialStyles } from './SocialStyles';
import {RiSpotifyLine} from 'react-icons/ri'

export const Social = () => {
    return (
        <div>
            <SocialStyles>
            <Container>
                <Row>
                    <Col><a href="https://www.facebook.com/" target="_blank"><FaFacebookF /></a></Col>
                    <Col><a href="https://www.instagram.com/" target="_blank"><FiInstagram /></a></Col>
                    <Col><a href="https://www.twitter.com/" target="_blank"><FaTwitterSquare /></a></Col>
                    <Col><a href="https://www.spotify.com/us/" target="_blank"><RiSpotifyLine/></a></Col>
                </Row>
            </Container>
            </SocialStyles>
        </div>
    )
}
