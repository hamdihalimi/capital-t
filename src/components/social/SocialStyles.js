import styled from "styled-components";

export const SocialStyles = styled.div`
    color:#113CFC;
    font-size:2rem;
    margin: auto;
    padding:2rem;
    text-align:center;

`