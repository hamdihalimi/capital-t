import React from "react"
import { Link } from "gatsby"
import { useSiteMetadata } from "../../hooks/use-site-metadata"
import { FooterStyles } from "./FooterStyles"

const year = new Date().getFullYear()

const Footer = () => {
  const {
    title,
    twitterUsername,
    facebookName,
    instagramName,
    siteAuthor,
    siteAuthorUrl,
  } = useSiteMetadata()
  return (
    <FooterStyles>
      <div className="container">
      <h2 style={{ marginTop: "0px" , fontSize : "2.6rem"}} className="title__main">
        {title}
      </h2>
      </div>
      <div className="container">
        <ul>
          <li>
            <a href={twitterUsername} target="_blank" rel="noreferrer nofollow">
              Twitter
            </a>
          </li>
          <li>
            <a href={facebookName} target="_blank" rel="noreferrer nofollow">
              Facebook
            </a>
          </li>

          <li>
            <a href={instagramName} target="_blank" rel="noreferrer nofollow">
              Instagram
            </a>
          </li>
        </ul>
      </div>
      <div className="container">
         &copy; {year} Designed and built by{" "}
        <a href={siteAuthorUrl} target="_blank" rel="noopener noreferrer">
          {siteAuthor}
        </a>{" "}
      </div>
    </FooterStyles>
  )
}

export default Footer
