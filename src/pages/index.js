import React from "react";
import Carousel from '@brainhubeu/react-carousel';
import '@brainhubeu/react-carousel/lib/style.css';
import { Card,  Col, Container, Row } from "react-bootstrap";
import Layout from "../components/layout";
import 'bootstrap/dist/css/bootstrap.min.css';
import { Link } from "gatsby";
import './index.css';

const Index = () => {
  return (
    <Layout>
      <img src="https://i.ytimg.com/vi/mMFXtPET3S8/maxresdefault.jpg" alt="capital" style={{
         width: "100%", height:"50%" ,
        }}/>
     
        <Container className="music" style={{ margin: 'auto' , alignItems:'center'}}>
          <Row>
          <Col   lg={6} sm={1}>
            <Link to="/music">
            <Card className="card" style={{ margin: '1rem' }}>
              <Card.Img variant="top" src="https://www.teksteshqip.com/img_upz/artworks_full/116/116306.jpg" />
              <Card.Body>
                <Card.Title >
                2Po2 & Capital T - High Records Vol. 2 (2008)
                </Card.Title>
              </Card.Body>
            </Card>
            </Link>
            </Col>
            <Col   lg={6} sm={1}>
            <Link to="/music">
            <Card className="card" style={{ margin: '1rem' }}>
              <Card.Img variant="top" src="https://assets.audiomack.com/cristian-ronaldo/8b711192c5e6cb3493781bc8ca113492a3e0e06fca74ed083c351cca6ddd0750.jpeg?width=1000&height=1000&max=true" />
              <Card.Body>
                <Card.Title >
                Capital T - Replay (2010)
                </Card.Title>
              </Card.Body>
            </Card>
            </Link>
            </Col>
            <Col  lg={6} sm={1}>
            <Link to="/music" >
            <Card style={ {margin: '1rem' }}>
              <Card.Img variant="top" src="https://i.scdn.co/image/ab67616d0000b273cd68d5f07c195ef8b757e010" />
              <Card.Body>
                <Card.Title >
                Capital T - Kapo (2012)
                </Card.Title>
              </Card.Body>
            </Card>
            </Link>
            </Col>
            <Col  lg={6} sm={1}>
            <Link to="/music">
            <Card style={{  margin: '1rem' }}>
              <Card.Img variant="top" src="data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wCEAAkGBxAQEBUQEBIVFRUVFRUVFRUVFRUVFRcVFhUWFhYVFRYYHSggGBolHRcVIjEhJSkrLi4uFx8zODMtNygtLisBCgoKDg0OGhAQGi0fHR0tLS0tLS0tKy0tLS0tKy0rLS0tLS0tLS4tKy0tLS0tLS0tLS03LTgtLS0tLS03LS0tLf/AABEIAOEA4QMBIgACEQEDEQH/xAAbAAACAwEBAQAAAAAAAAAAAAAAAQIEBQYDB//EAEMQAAEEAAQDBgMEBwYFBQAAAAEAAgMRBBIhMQVBUQYTImFxgTKRoSNCscEHFVJictHwM0OCsuHxFFOSotIWJVR0k//EABsBAAIDAQEBAAAAAAAAAAAAAAABAgMEBQYH/8QAMhEAAgEDAwMACQMEAwAAAAAAAAECAxEhBBIxBUFREzJhcYGRsdHwIqHBFDRC4aLC8f/aAAwDAQACEQMRAD8ArheWKxUcQt7gPLmfQL0tch2giqd186I91ihHc7Ht9fqZaalvir5sWcR2kkJ+zaAOV6kr1wnaJ2cCUCtiRYr2XPh9DT3/ANFJwoAk6nX0HVaPRR8HmV1PUqW7f9jtJOKx5izoBr1vkKVmCYO6LhDI6657f10WxwnioALJCBWx19781TOk0ro62m6uqk9s8X4OqCKXhgZGyMDmkEG1YpVHajJSV1wFKJCkAhAEaSIUikUDI0oUvS1ElA0RK8yvVebkiSEVAqaiQkyZBNFJ0ojEhCEACEk0gBJCaBEUIQmAITtCQHu4aLA49hS/KW2XdPLyXRqDmc2jVWxbi7oyamhGvTdOXc4DIbIcCCN/I+YUXEncnYDXoNAFa4nA6OU5hV+LTQG9691WA6remmrnhqkHCbi+w3POa/O9vO1OWRznZjudyOfUnqVNjFA6bafggLCZiHt+Fzhz0JCuRcbxLf7wn+KiFRKmIfYjcFJpPklGrUh6smvizo8B2lB0mbX7zdvl/JbsMrXjMxwI8ivn7XUeXmOXrpuFbw2IdGQ5hrTQfy6jyVUqS7YOrpusVIYq/qX7/wCzuFErO4bxZsnhdQf8gfTz8lpFZ2mnZnoqNaFaG+DuiFotCRUS4CvMqZUCgkiJUVIqKiTEmUkkhjQhCQ2JCSaBCTSTQAIQkmIaEkIAtgrzxeKbE0vdsPr5Bei5Di+M72RwvwtBAHLTn6q6nHczndQ1a01Lcst4X57CvxHHmeTMdBs0dAvFnRebApnVbEksI8dKpKcnOTu2eoeF4uCD0QhIi2Jt8lejLntFVbdAK1y8/UDp5mksDA11HmNSDzHULWxfDctuG2l1qNf65aJgVBgGS7HK7cA7H3H9H5on4e5lA02jqNqPW/Yf0F7PNjQ7c6N77kc/NKXiBc3K/wAVg9aPXU8xp8kCKbQ0kC6IFgjkeX1pdFwzH5/A/wCL7pOmYDf3XKyZybBOmgvoeXmrDZXPAo04aj58vf8ANQnDcjXpNXPTz3Lh8rydiQkVS4Tj++Zro5vxD8CrhWNqzseypVI1YKceGRKiVIqBKRciJSKCgqJJEU0k0iQIQhIQKKkkgBJoQgAQhCBAhCSYBxPE93E53PYepXFgWdSt/tXJ8DPVx/AfmsGbTQ8hXv5eS3UY2jfyeQ6zWc9Rs7R+vciwCtfZBcOqi95P+iiWlWnI9xMOXtC0OOo3VcNPQr1iDuhQNGvg25RpuPhPXUaev9c9PZ+LLQRsDuDpR9P9lQikrck2NR18x5py44k75vM7pDLTT4bFAjmPLTUcuSpvbeg9Qui7M8Gkxb/Bdc3EfOiu9g7C4YNpzbPWqVbnZlihdHx0ucaLeo+fL8woPkIrnz+eq+j8S/R0MxML6BvQrk+K9k8VC74LHUaqSqRZF05IzOHYwskDr3NO9D/Jdfm0tcJK0t0cKIK67hcmaFh8q+WiprLhne6JVf6qT9/8FolRcmVErMejEVFSKgkTQJpIQwGkmhIGJNRQgQ00kIECEIQAJJpIuBzvaN57/wAg0D53/Mp8P4I6TxvNMHzKt8Woz6jYD576/RXsOXyOawakkbD+vJblO0UeL1lJPUzbzkb8LhoGZyAB1Op8gB1Rg8XhZTlzjXkW1+K68dno6aX4cYqQCw2R1RMvy2c4+nLlzhLwTh2Oa7Duw7cJigNA1oY4HqAKbI3+tFWpJ83IOlNZSRmQ4OPQAAt21APvatQ8Mj1Aa3yAo6LhMRPi8BO7DyE2w0WnVpHJzb5EbLo+C9qGvGRwp3nz15Kbi0ilTTduDYl7N4aQaso7kh1EjqvF/YvDDYuv97+W6ux403Zs9b1r1V/DzZhZG/PW+mvRR3D2mj2ZwjYWZG0B0qj7row61zeExetc/XWvRbEM2ig2SSPdwVSeEHde5kUHuUXksRxHafsjFM1zmeF+/kSsCHDd00M6AfgvomLdouJ4r/au9vwChufB1elRXpW/YVCoFMlRSPQjUUyopEkNJNCQCTQhIBJJpoBiQhCZEEIQgAQhCAMqOB0jy+tXO0H4LsOEYARZXEeNxA8wCfxVzDcJZFqBZ6nkq+NxGSRhOwez/MFc5XPIW3Su+5102MhwrDLK4Na2hfV3IBcb2g4lDjZIZoRO2RsrMjxE4MrMLzPNCqv6rrhle4skjDix2dgcLBsCj7arPxDMVJNHG4MZDm8Qo2GgHRvLp80k7cCt3M7tlwmLFQ965oL4/vDQ5OYv6r5fPwl2piOcD1BB6Dqvu+LgibE+h4cp06il8YfM5rrbqbIFc9vl/oraUpIy1YxlmxX4Z2hlgOSUF7Ru06OHv/Nd7wPGxYsfYyAu3LXGnj/D0Wb2f7PxY5n2zDp/eC7J3IB50sjtF2MnwTu9icXMBsPbo5vrW3qFOThJ24ZCKnHjKPomDwrwdeXsVrwWF824D2+mhAbjWmWPbvm/GP4hz+h9V9H4ZjYMTH3uHka9p6HUeThuD6qmVOSyWqonjhnq+ReffL0mYqklAKt8liyjN4ji91yeJlzOLupWx2hxrI225wFkNBPUrDclbB3elwVpS7kXJJlRQdiwIQhRGCEIUQBCSaAEhCaYhJITTASEJpCEhCEAfQJW7rkO07q+a7CVy5TtPDmaVNcnkYHZ9neJR4jDRy6Z8tE8wRofwVHiOLLpWlzyCwuAYG21+Yb5uVVsvmfA+OTYd5aw6E/Cdr8l02L4yYmCaZwzH4WVTR7dVZKLvYinH1jb7ScaZDCYy7xvBAaN9unrS+c8OgMzmi9bOw8vFXTelT4jxN8s7pXbu5DpVUOgXY9gsEBTyNRp7bqy2yJlvvl7DueB4JkMbWgVTQK9B+Kt4oggirB3XjG6tFN+uiocsF1snyztdgIoJS+J2XNu2tD7bUsPCz92e9w0xhk5tBIa4dARt6H6L6F244WJISQ2yNQvlceIkiPhNeRAP0IWii7rnJVXaxdYN9nbLGix3vi5h9Ef4T+R+quYfthO4VN4d7cQRtrQHXULmsXh35e9loF3wtAAJ8yAKA/FTws/9myQtyNJdWVpJAs1dW4kmtfyVjhFrgqpylvSv8/5NfEcZgxDXDEXV+AAGx+9fIqhw/FyR5u6zSRNr4hRF3QHQ0HeWh2VbHYszOsgNYPha0AAetbldDwrDFkLWuGupIPKzdKE7QWUdTRwqairanLbsXrJZfsfs8XPeCYPaHi6IsXoVMppLI2erimlnkSaSaQwQhCiAIQkgAQkpJgRQmhMQJJoQAIQhAHdPcsXjothK3Mthcz2wlyQE3RJ0Ukrs8fdLJxmHe2Jxe4a3oFX4nxIzPJO3JZ805cSSbtebSt6guTnTrXVlwWYBbrK+q9kYGsiFWSdT6Db81844EwPlGYWPPUDz9l9b4dAGNFGxSorvsX0FyzQYUPlpeYevM9VnL7HljXZ2kfivmnHYI4JC93iO4HnyAX0PH4jIwk/6+y+cdocO6ZzZA4Zs7m90bsaiq/NTpLITT2YWTGJkldneSS4gADcnYNY36LW472bdhYIpZD9pI4gsGzGht1fM7arsexfZAxPbPOWuf8AdAIIaPLzWl29wEckcRcLyPOl6at5jnstDqWZVR0znJQ7vz/J844Lw7MRI4eEatH7R5H0H1W4pFRKzTnudz12l0sdPT2R+L8iQmoqDNY0JJpACEIUQBCEkwEpJJoASSaSBAmkmmAkIQkB3xK4/wDSESIWjkXG11j3Lk+2hDoq3IOnurYYkjxsldM+buTTe2jSja6JyTW4FicsrRsL6BfVMLjNAV8YY8g2NF2/ZzipeynHUaaLLXg+TZppr1WdscSUS4vTzWQ3HtG5Wfx/iBfEWsBs1rsfZZkrm1o08dxKGLxTPBcBYb0PIke4Xzfi3EO+mdJWW+hI22J81MYSZ7qyknr191a4fwyzT4wKOoddrVFRhnkojSq13tijQ7McWxbW52ylzAayO19wVu8R4rJOAH0K1odVnwwtYMrRQ6Dqgqicrs9LpNBCkot5ku/tApFCHKs6IkISQMEIQogNJCEACEIQAJoQgCKkopoEJNJCBDQkhA7naSO0XL9pXW02rX66c7R7R7fksvi787SWn2P81OPJ5uWkqwy4nG4tnNViVZnfRIIVUrox4OBWtubQ1YwGMdC4OafZVk02rlSduDv+GYuHFCgcrv2b19bVjHcKkyExkGhta+eRylurVbj4vM0EB51FHVZ3Qzg2x1btk1eGYmd8hbYaGnXyW3X+65Lg2ILXlwPz5rpocSHeqrqqzsd/pE6cqd362T1ckU0lSdsZKimpwQue4MYCXOIDQNyTySHdJXZ5pLs4f0dYktt0kTT+zbj83ALGd2ZxDcSMK8Na9wsEnwOHUED8lN05JZRkh1DTTvtmnbkxULW49wCbBOY2YsOcEjISdqu7A6hX8H2MxEsLZw+INe0OGZzgaO1jKoqnJu1uCb1unUFUc1tlw/JzSF02F7E4mWETtdFlc0Poudmoi9Rk3WVw3hD56LXsF5viJFBo+J2mgvT1Q4SXYI63TyTamv0884M5Cv8ADOFyYiXumUHDcm6HiDeQPMr2xPApY5mQF0ZMjczXNJLCLI3q/unkltdr2Jy1NKM9jkr2v8OTLSW//wCk8SR4Sx3iynLnNGrs+HQeazn8JnE4w+Q944gNHW9iD0318kbX4Ix1dCd9s1j6FFC7Mfo6xGWzLHf7ILj9aXN47g08U/8Aw72XISAA02HXsWlOVOUeUQpa7T1ZOMJptFBJdbh/0f4tzcxMbT+y5xv5gELnOI4CXDyGKZuVw9wRyIPMIlCUctDo6uhWk405ptFVCaFA02I2g0mQlSZCxjcXwY+Ie6wnMXZTRBworCxvDSNQtdGrizPNdT6fLd6SC+RjkUgFehj1Xo2ILTc4Kg2V0L2dB0UGwkmkXQnCXgs8OjJdot3DtIIRw7AiNvUlXAxYqlRN4PU6DQypwTlzyegk8l6rwpSaVTc7cW+56Lb7FSsZjoTJQFuAJ2zOaQ36mvdYgVrhmCfPK2GOs77y5jQsAu39AVKHrKxXqYxlRnGbsmnnwfXePOnjDJ4Wl/d5s8QdlztLavY2QaNeq5rhfaD/AI/HQHuu7MQk+9mJDgNNhWy2uAYjEw5MNjcpc4OET2uzFwYLLX6bgc+dfOP6oji4gydlAyRyZwNBmBZ4gOVg6rbJN2a+R46jKlTU4TScrPbJN/mc+3scx+kzFZ5o25HAMDxmcKDiSLy9QK3VzhHF8c3CxNbBGYwYo2uJNua7TMQNuWvmvP8ASoPtMP6P/Fq6PguH7zh2HFltMjdba+7RrXkapVWbqSyb51Ix0FByimrvm/llPhczYcGGRtcCYI5Xl2rXZ48tNN6Vl6LA7L8Dilijke8ttzswPwubmIA03+HY+a6/hABwDMwFf8MzcXpk58vZVeyMMbeHQyPHwse/01dZrrVqTjdq/gxqu6dOptum5L6SDs7wxmGnxMpoCR4bHqPhAzGvcn/pVLERZ+KxZgKEOZoH3RmqvPc/NavHuE5owIrDiabqfU/QFZHCsHKziLWzauGHcQRzAe2qKUrpqNsXCnPep1XL9W1q3fhI3pHlmOiiZox8MznNHNzXRAH6n5qvxERt4lBI6h9hKLOmocwN/wAzh7r2xA/9xh/+vP8A54VV4zE6THQtYQD3Ex11HxxaH6/JWPv7/sZ6fKza8JX/AORdxeOLcbDCHU10cpcOrgWZfxPzXjjsO39Y4d/Pup/asgsefiKx8ZwUjF4aNrnNpkzs1ucTq0ltuN14uq1O7czHwB7832WIr5xf17JJtt3Xck4Rik4S5i/+w+N8Udh8VCSJHRGOTM2Nhf4szMpIH+L5rhu3vEY8RLE+NkjKa4O7xmQnUEVe/P5r6PJjaxccVWJIXuu9u7c3/wA/ouK/SuftcP8AwS/5mKFZPa3fBs6U4rU0045s839/a38nCpIQsNz1xIoQhSASi9lhTQgTSfJg43hpslqzXsI3XWlqrTYNruS0QrNcnE1XSVLNPBzVq9w+DObKuv4Y3krOFwwYFKdZNYM2m6dUjVW/gsMbQU6TQsp6NJJBSEJIJACvfC4h8T2TREB8bg9tixY5EdCLB9VXKmxOLaZGUFOLhLh4Ovb25sHPh3teTYyOY5pO5D3O1okkWANF5u7cSuxAxD4fC1jo2RNcDWYtJc4uqzTdtOXquWXSYXBtGAe5wFuDnjqNKaR8vqivrHTSb7tI41TpelorKb3Y54v4NKb9IAkeLwOZgB0d3bpL6gXlA25/JVOAdt5cO3uXwd6yz3eVwa9ocSchDtHCzobFbaqPZfCBkbp36WDRPJg3PuR9FLBGN72xuwxY4vMocWgDKHZhrv8AsilRLqclOSSvt7lMtFpY76ai2lbN1yr/AIzo+JcVkflhDWtEoc19OcC2IDUNA0zWQ3NytY/F+Py4fDOwkcH2WTuRKTY8TaPO7FnlySbi/FPPuI/sWebhqa9XED2Wfi8FiXOZhXyhzXXK46gijRvrqdFloaysp3nLFs3+bt7VgqpaOldKeErN5fZZ+SLD+3zy6AmF32OYv1aA9xbkbl8V6AuJutSE39uWOxTcScPNTIXR5biDnFzw6x4yKAGtlZ36qglgfLFna5ji3xEHNVcq03Wp+qMMyeIZNa+HcEgXmIPT8XBbJdSti37eEWT0mjjfEr/qVv3Lx/SZHuMHNfm+Ee15liS9occ7FNxroC0NGRkdPLQw3Yc+hbnE71W29a+2Dhha/EYvIMkZc2NvLMANQPM1816hjpRh3PLjLLIHkZiGiNpzfDtQGVKfUJvtx9r/ALdxU9Jp6UrqLaas9z8xu1jwuWbeC7S1O8Pw8wkyF4a7uyC2xo1+et6Autd6WHHx3F4niLJRF3XdgtEb9SI78ZkI5nTboN0sbiA+bMdnzRQs82RvDpD6F1D2V9uIyzyveQ1zm93C12mbu7JPoXH6KmOqqRhtS5z8SqOnhTvJxu3FrPHb52V/da5bk7XwwMlyQF4gysBBaLdI421rnG9Ks+mlrgeM8Wnxk5nmoeEMZG3UMaDZ15kk7+i055e6YTKI2kB3dwMogOcKMklE7Anc2sBaoVpyjZu50dD0+lCfpUrtcZuuM/8AoIQhGTsDQkhMRTbxOLMWudlcNCHCvrsqWJ40BK1raLAacet6aeQ39lLtDhWFneHRwoD97yXNrVSpQktx5vqPUNTQn6LHZ3XLXh+Pb5O8KS57hGKxRoNGdu1u0A9Hf7roCFRODg7M7Wl1S1MN6TXv/jyJSCEKBpBCEJACEJWmMSbEipIEj0aLIA3OnzXY4tudrsMzQNijDj0BcLPs0E+65DByBsjXHWnNNc9CCtcuxDnTOa3K2Xdz/BTTtRJ6aLHqoOUo5StnPm6/i5k1UHKSza31v9rmlxDFB0EUUQ/tHBrQP+WD+Yr5lWJ8SQ+Z4GkEJaP43U4j6NWO3GMjY1jpWlzAWtdG0uc0HcBxpo9UP48A7M0PJAI1eGjXclrBqVk/ppPEVfn43f2Ma08nhRus/HP2LjcM8Q4eM6DOZpnHZtHNTjyOo+S9Mc175JMrmtkcWd00n7kbgbvYWda8liu40+iGsjAJs6F1nqcxNnzXu7jgr+9d6yNYPlG0K10Kqd7efrf89hZLT1r3su/1ubLn92Gxsgc7M4PlLQ4RhxcCcpI8Q8tlVMkpxLppAGsyujbckbXBp2Is78/dY8nGDyjZ/jL5D/3OXn+t5h8Ja3+GNg/JSjp5rss+0cdHUV8K7Vuf9m3HMQx0N4cxEABhe5xFG7JYNSTqVF00tAd/RGzmQSlwF3kDq+HbTyWI7i+IP96/2JH4JfrSf/nSf/o/+as9BJeCX9FPyvz3ou4x8T33NPKXDT+yDa56AuFLylkwrjb34l56nuvzJWbLK5xzOJJO5Js/NRpXqnjlmqNCySu8eLK3ySNES4MbRyn1exv4MSOLw4+HDX/FI8/QUs9K1LYvb8yToJ8tv4v7mn+s2f8AxoP+l/8A5oWbaSjsj+N/cPQU/H7v7jCFiRdom/fYR6G/xpXYeLwO+/X8Qr67LS6U1yjPS6jpqnqzXxx9SOO4b3zgXyENGzQPmV4zdn4y2mkh3Um/mFqtIIsEEeWqaSqTWLjnodNVblKKbfczeBQvja6N4oh1joQRuD7LSQhKctzuXUKSo01TTukCSaFAtEhNCBgVFBQSmJuwr1Vrh04Y/MaIogg8wasetWqw2UJpmsbbjSUo7lbyVztse947mri8QwMjbE74HvIOzqOXK4+ejvS1Ulmc825xd5kk/isZrHyHOSQOQ8vNaUJ0pPYo+8q0093+Pub5ZcxMgLIwDZa0g77l7iPoQq6FFCVjVGNlYvYiZromi/G3R2nxN3br+7qPNUkISjGysEIKKsi1Fiahc3NqS2h+74s34hVUISSsEYKLbXcs8PlYx+Z4sDSuoOjj8r+a8JWgOIabFmj1HIqKEJZuJRtJy8ntgJA2VjnbBwJ56A6pYmbO69NAGiuYHMnmV4Jo25uGxbt3cbVa4pO17yWmxbq32NdRp6eSqIRbNwcU2n4BCSFInc4QoKELqHzZmn2a/tSupKSFgr+sez6P/aoHJoQqjqMEBJCQhoQhAAVE/mhCYmAWTxrdn8SEKVL1kY+of28vzuanJJm6EKKLl/j7ke5SQhM1IEkIUQGhCEwIoQhHYiNJCEhghCEACEIQB//Z" />
              <Card.Body>
                <Card.Title >
                Capital T - Slumdog Millionaire (2015)
                </Card.Title>
              </Card.Body>
            </Card>
            </Link>
            </Col>
            <Col  lg={6} sm={1}>
            <Link to="/music">
            <Card style={{ margin: '1rem' }}>
              <Card.Img variant="top" src="https://img.discogs.com/X4GOTlhAn33s5is3yYSQRaew2k4=/fit-in/600x600/filters:strip_icc():format(jpeg):mode_rgb():quality(90)/discogs-images/R-11516464-1517733528-1717.jpeg.jpg" />
              <Card.Body>
                <Card.Title >
                Capital T - Winter Is Here (2017)
                </Card.Title>
              </Card.Body>
            </Card>
            </Link>
            </Col>
            <Col  lg={6} sm={1}>
            <Link to="/music">
            <Card style={{ margin: '1rem'  }}>
              <Card.Img variant="top" src="https://abload.de/img/500x500-000000-80-0-0suj3w.jpg" />
              <Card.Body>
                <Card.Title >
                Capital T - Skulpturë (2020)
                </Card.Title>
              </Card.Body>
            </Card>
            </Link>
           </Col>
            
         </Row>
        </Container>
        <div className="event" style={{margin: 'auto' , alignItems:'center'}}>
        <Container >
        <Row lg={12}>
        <Col >
        <Carousel  interval={1000}plugins={['arrows']}>
        <Link to="/event"><img src="https://direct.rhapsody.com/imageserver/images/alb.356864834/500x500.jpg" /></Link>
        <Link to="/event"><img src="https://direct.rhapsody.com/imageserver/images/alb.356864834/500x500.jpg" /></Link>
        <Link to="/event"><img src="https://direct.rhapsody.com/imageserver/images/alb.356864834/500x500.jpg" /></Link>
        </Carousel>
        </Col>
        </Row>
        </Container>
        </div>
    </Layout>
  )
}

export default Index
