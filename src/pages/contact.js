import React from "react"
import {  Button,Col, Container, Row ,Form} from "react-bootstrap"
import Layout from "../components/layout"
import './index.css';

const ContactPage = () => {
  return (
    <>
      <Layout>
      <img src="https://i.ytimg.com/vi/mMFXtPET3S8/maxresdefault.jpg" alt="capital" style={{
         width: "100%", height:"50%" ,
        }}/>
        <div className="contact">
          <Container>
           <h1>Contact</h1>
            <Row>
            <Col sm={6}>
            <Form>
             <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
             <Form.Control type="text" placeholder="Name" />
             </Form.Group>
             <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
             <Form.Control type="text" placeholder="LastName" />
             </Form.Group>
             <Form.Group className="mb-3" controlId="exampleForm.ControlInput1">
             <Form.Control type="email" placeholder="Email" />
             </Form.Group>
            <Form.Group className="mb-3" controlId="exampleForm.ControlTextarea1">
            <Form.Control placeholder="Message" as="textarea" rows={3} />
            </Form.Group>
            <Col  className="buttom_contact" lg={12}>
            <Button variant="primary" type="submit">
              Submit
            </Button>
            </Col>
             </Form>
            </Col>
            <Col  className="contact_info" sm={4}>
              <p>Email : capital-t@gmail.com</p>
              <p>Tel : +383 49 226870</p>
            </Col>
            </Row> 
          </Container>
        </div>
      </Layout>
    </>
  )
}

export default ContactPage
