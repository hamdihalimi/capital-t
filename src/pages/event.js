import React ,{useState} from "react"
import { CardGroup, Container, Row ,Col, Card, CardImg ,Button,Modal, ModalFooter } from "react-bootstrap"
import Layout from "../components/layout"

import {BsFacebook,BsInstagram} from 'react-icons/bs'


const EventPage = () => {
  const [show, setShow] = useState(false);
  const [show1,setShow1] = useState(false)

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const handleShow1 = () => setShow1(true)
  const handleClose1 = () => setShow1(false);
  return (
    <>
      <Layout>
      <img src="https://i.ytimg.com/vi/mMFXtPET3S8/maxresdefault.jpg" alt="capital" style={{
         width: "100%", height:"50%" ,
        }}/>
        <Container>
          <h1 style={{textAlign:'center', margin: 'auto'}} >Events</h1>
          <Row>

          <Col   lg={6} sm={1}>
            <CardGroup>
              <a  onClick={handleShow}>
              <Card style={{margin : '1rem'}}>
                <CardImg variant="top" src="https://direct.rhapsody.com/imageserver/images/alb.356864834/500x500.jpg" />
              </Card>
              </a>
            </CardGroup>
            <Modal  size="lg" show={show} onHide={handleClose}>
          <Modal.Header closeButton>
          <Modal.Title>EVENT 1</Modal.Title>
          </Modal.Header>
          <Modal.Body><img src="https://direct.rhapsody.com/imageserver/images/alb.356864834/500x500.jpg" style={{
         width: "100%", height:"100%" ,
        }} />
        </Modal.Body>
        <ModalFooter>
        <a href="https://www.instagram.com/" target="_blank"><BsInstagram style={{color:'blue', fontSize:'2rem'}} /></a>
        <a href="https://www.facebook.com/" target="_blank"><BsFacebook  style={{color:'blue', fontSize:'2rem'}}/></a>
         Lorem Ipsum is simply dummy text of the printing and typesetting industry.
         Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
         when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
        </ModalFooter>
        </Modal>
        </Col>

        <Col  lg={6} sm={1}>
          <CardGroup>
          <a  onClick={handleShow1}>
          <Card style={{margin : '1rem'}}>
          <CardImg variant="top" src="https://direct.rhapsody.com/imageserver/images/alb.356864834/500x500.jpg" />
          </Card>
          </a>
         </CardGroup>
          <Modal size="lg" show={show1} onHide={handleClose1}>
          <Modal.Header closeButton>
          <Modal.Title>EVENT 2</Modal.Title>
          </Modal.Header>
          <Modal.Body><img src="https://direct.rhapsody.com/imageserver/images/alb.356864834/500x500.jpg" style={{
         width: "100%"
        }} /></Modal.Body>
        <ModalFooter>
        <a href="https://www.instagram.com/" target="_blank"><BsInstagram style={{color:'blue', fontSize:'2rem'}} /></a>
        <a href="https://www.facebook.com/" target="_blank"><BsFacebook style={{color:'blue', fontSize:'2rem'}}/></a>
         Lorem Ipsum is simply dummy text of the printing and typesetting industry.
         Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,
         when an unknown printer took a galley of type and scrambled it to make a type specimen book. 
        </ModalFooter>
         </Modal>
        </Col>
        
       

        </Row>
        </Container>
      </Layout>
    </>
  )
}

export default EventPage