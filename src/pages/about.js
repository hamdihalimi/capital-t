import React from "react"
import { Container, Row ,Col} from "react-bootstrap"
import Layout from "../components/layout"
import './index.css'

const AboutPage = () => {
  return (
    <>
      <Layout>
      <img src="https://i.ytimg.com/vi/mMFXtPET3S8/maxresdefault.jpg" alt="capital" style={{
         width: "100%", height:"50%" ,
        }}/>
      <div>
        <div   style={{padding: '1rem'}}>
        <Container>
          <Row className="box">
            <Col lg={8}>
            <h1 className="tittle">ABOUT  TRUEKAPO </h1>
            <div className="content">
                <h3>Life and career</h3>
                <p>Trim Ademi was born on 29 February 1992 into an Albanian family in the city of Pristina, then part of the Socialist Federal Republic of Yugoslavia, present Kosovo.[1] His father,
                   Agim Ademi, is a former professional football player and current president of the Football Federation of Kosovo.[6][7] 
                  Ademi attended the Asim Vokshi Elementary School and enrolled, upon his graduation, 
                  at the Sami Frashëri High School in Pristina.[8][9]
                  Ademi commenced his early music career in 2008 and debuted with the single "Shopping"
                  by 2po2 featuring Capital T. The same year, he released his second single 
                  "1 Mëngjes" in collaboration with Albanian singer Adelina Thaqi.[1] The following year, he released almost three singles including "Shum Nalt" in collaboration with 2po2 and Albanian singer Dafina Zeqiri. In 2010, he announced his debut studio album Replay and released the record in the same year. That year, he participated with Eni Koçi in the 11th edition of Kënga Magjike with the song "Diva".[10] The singer released his debut studio album "Replay" in 2010, his second album "Kapo" in 2012 which was followed by his third album "Slumdog Millionaire" in 2015.[1]
                   His fourth album "Winter Is Here" was released through Authentic Entertainment in 2017 and achieved commercial success in Switzerland whereas it debuted on the Swiss Album Charts at number sixty-nine. 
                  In June 2016, he won the award for the "Video of the Year" at the Top Music Awards 2016.[11]</p>
                  <h3>Activism</h3>
                  <p>Ademi has made contributions to various charitable causes throughout his career. In March 2019, he visited the Down Syndrome Association Kosovo to raise awareness and acceptance to people with the down syndrome.[39] In November 2019, Ademi expressed his condolences via social media following the 6.4 magnitude earthquake in Albania.[40] He subsequently went on to visit Albania to raise funds for rehabilitation, reconstruction and to support several campaigns.[41][42] In December 2019, Ademi joined the UNDP Kosovo campaign #EcoKosovo, aiming to mobilise the country's population towards a healthier environment.[43] 
                    In May 2020, he extended his support to the Black Lives Matter movement in connection with the
                     wider George Floyd protests.[44]</p>
                  <h3>Discography</h3>
                  <ol>
                    <li>Replay (2010)</li>
                    <li>Kapo (2012)</li>
                    <li>Slumdog Millionaire (2015)</li>
                    <li>Winter Is Here (2017)</li>
                    <li>Skulpturë (2020)</li>
                  </ol>
              </div>
            </Col>
            <Col lg={4} >
            <img src="https://www.revistawho.com/wp-content/uploads/2019/11/photo.jpg" style={{width:'100%', marginTop:'150px', alignItems:'center'}}/>
            </Col>
          </Row>
        </Container>
        </div>
      </div>
      </Layout>
    </>
  )
}

export default AboutPage