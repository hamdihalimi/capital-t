export const NavItems = [
  {
    name: "Home",
    path: "/",
  },
  {
    name: "Music",
    path: "/music",
  },
  {
    name: "Event",
    path: "/event",
  },
  {
    name: "About",
    path: "/about",
  },
  {
    name: "Contact",
    path: "/contact",
  },
]
